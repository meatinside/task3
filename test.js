const task = require('./index.js');

QUnit.test( "task test", function( assert ) {
  var expected = [[1,1],[2,1],[2,2],[3,2],[3,3],[4,3],[5,3],[5,4]];
  assert.deepEqual(task([1,5,6,2,4],[3,8,4,6,2]), expected, "Passed!" );
});
